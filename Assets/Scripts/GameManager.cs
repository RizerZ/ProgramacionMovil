using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public Transform ParentAtCreate;
    public int InstantObjAtStart;
    public GameObject TapToStart;
    [Space(50)]
    [Header("Prefabs")]
    public GameObject[] objectsLvl1;
    public GameObject[] objectsLvl2;
    public GameObject[] objectsLvl3;

    public int lvl;


    [Space (50)]
    [Header ("Prefabs in Scene")]
    public List<BlockCreated> CreatedPrefabs = new List<BlockCreated>();

    [Space(100)]
    [Header("Score Options")]
    private int TimeMin;
    private int TimeSec;
    private string TimeRecord;
    public int Items1;
    public int Items2;
    public int Items3;

    public Text ScoreText;
    public float Distance;
    public Transform StartPointAtDistance;

    [Space(100)]
    [Header("Dead Options")]
    public GameObject DeadPanel;
    public GameObject StartPanel;
    public GameObject MenuPanel;
    public GameObject EndLvlPanel;
    public Player PlayerScript;
    public float StartDistanceInMenu;
    public Text ScoreInMenu;
    public Text DeadScore;
    public Text TimeToWin;
    public Text RecordInWin;
    public Text ButtonText;

    public int escalaTiempo=1;

    [Space(50)]
    [Header("Others")]
    public Clock clock;

    void Awake ()
        {

        if (Instance == null)
            {
            Instance = this;
            }

        }
    void Start()
    {
        lvl=FindObjectOfType<Nivel>().lvl;
        if (lvl == 1)
            {
            ScoreText.text = "Items: " + Items1.ToString ();
            TimeMin = PlayerPrefs.GetInt ("TimeMin");
            TimeSec = PlayerPrefs.GetInt ("TimeSec");
            TimeRecord = "Time Record: " + TimeMin.ToString ("00") + ":" + TimeSec.ToString ("00");
            ScoreInMenu.text = TimeRecord;
            StartCoroutine (StartAtCreateTerrain (objectsLvl1));
            }
        if (lvl == 2)
            {
            ScoreText.text = "Items: " + Items2.ToString ();
            TimeMin = PlayerPrefs.GetInt ("TimeMin");
            TimeSec = PlayerPrefs.GetInt ("TimeSec");
            TimeRecord = "Time Record: " + TimeMin.ToString ("00") + ":" + TimeSec.ToString ("00");
            ScoreInMenu.text = TimeRecord;
            StartCoroutine (StartAtCreateTerrain (objectsLvl2));

            }
        if (lvl == 3)
            {
            ScoreText.text = "Items: " + Items3.ToString ();
            TimeMin = PlayerPrefs.GetInt ("TimeMin");
            TimeSec = PlayerPrefs.GetInt ("TimeSec");
            TimeRecord = "Time Record: " + TimeMin.ToString ("00") + ":" + TimeSec.ToString ("00");
            ScoreInMenu.text = TimeRecord;
            StartCoroutine (StartAtCreateTerrain (objectsLvl3));

            }
        
        Application.targetFrameRate=60;
    }

    IEnumerator StartAtCreateTerrain(GameObject[] lista)
    {
        for (int i = 0; i < InstantObjAtStart; i++)
        {
            var CreatedObj = Instantiate(lista[Random.Range(0, lista.Length)], new Vector3(0f, 0f, CreatedPrefabs[i].transform.position.z+20), Quaternion.identity, ParentAtCreate);
            var Data = CreatedObj.GetComponent<BlockCreated>();
            CreatedPrefabs.Add(Data);
            yield return new WaitForSeconds(0.2f);
        }
    }

    public void Generate()
    {
        if(lvl==1)
        {
            StartCoroutine(CreateMoreZones(objectsLvl1));
        }
        if(lvl==2)
        {
            StartCoroutine(CreateMoreZones(objectsLvl2));
        }
        if(lvl==3)
        {
            StartCoroutine(CreateMoreZones(objectsLvl3));
        }
    }

    IEnumerator CreateMoreZones(GameObject[] lista)
    {
        for (int i = 0; i < 1; i++)
        {
            var CreatedObj = Instantiate(lista[Random.Range(0, lista.Length)], new Vector3(0f, 0f, CreatedPrefabs[CreatedPrefabs.Count-1].transform.position.z + 20), Quaternion.identity, ParentAtCreate);
            var Data = CreatedObj.GetComponent<BlockCreated>();

            CreatedPrefabs.Add(Data);
            yield return new WaitForSeconds(0.2f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerScript)
            {
            if (!PlayerScript.isDead && PlayerScript.GameIsStart)
                {
                PlayerScript.Speed = PlayerScript.Speed + Time.deltaTime * 10;
                PlayerScript.MoveSpeed = PlayerScript.Speed + Time.deltaTime * 10;
                }
            if (PlayerScript.isDead && PlayerScript != null)
                {
                GetTime ();
                DeadScore.text = "Dead in: " + TimeRecord;
                DeadPanel.SetActive (true);
                StartPanel.SetActive (false);
                //Destroy (PlayerScript.gameObject);
                }
            }
    }

    public void GetTime()
    {
        TimeMin = clock.ReturnMin();
        TimeSec = clock.ReturnSec();
        TimeRecord = TimeMin.ToString("00") + ":" + TimeSec.ToString("00");
    }

    public void StartGame()
    {
        MenuPanel.SetActive(false);
        StartPanel.SetActive(true);
        DeadPanel.SetActive(false);
        PlayerScript.GameIsStart = true;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void WinLvl()
    {
        ButtonText.text="Next Level";
        GetTime();
        TimeToWin.text = "Level Completed in: " + TimeRecord;
        if(clock.ReturnMin()<=PlayerPrefs.GetInt("TimeMin")|| (PlayerPrefs.GetInt("TimeMin")==0&&PlayerPrefs.GetInt("TimeSec")==0))
        {
            if(clock.ReturnSec()<=PlayerPrefs.GetInt("TimeSec")|| (PlayerPrefs.GetInt("TimeMin") == 0 && PlayerPrefs.GetInt("TimeSec") == 0))
            {
                GetTime();
                PlayerPrefs.SetInt("TimeMin", TimeMin);
                PlayerPrefs.SetInt("TimeSec", TimeSec);
                RecordInWin.text = "New Record: " + TimeMin.ToString("00") +":"+ TimeSec.ToString("00");
            }
        }
        else
        {
            RecordInWin.text = "Record: " + PlayerPrefs.GetInt("TimeMin").ToString("00") + ":" + PlayerPrefs.GetInt("TimeSec").ToString("00")+" Try again to get better";
        }
        EndLvlPanel.SetActive(true);
        DeadPanel.SetActive(false);
        StartPanel.SetActive(false);

        PlayerScript.GameIsStart = false;
        }

    public void RefreshScore()
    {
        if (lvl == 1)
            {
            Items1 -= 1;
            ScoreText.text = "Items: " + (Items1).ToString ();

            if (Items1 == 0)
                {
                WinLvl ();
                FindObjectOfType<Nivel> ().lvl++;
                }
            
            }
        if (lvl == 2)
            {
            Items2 -= 1;
            ScoreText.text = "Items: " + (Items2).ToString ();
            if (Items2 == 0)
                {
                WinLvl ();
                FindObjectOfType<Nivel> ().lvl++;
                }
            }
        if (lvl == 3)
            {
            Items3 -= 1;
            ScoreText.text = "Items: " + (Items3).ToString ();
            if (Items3 == 0)
                {
                WinLvl ();
                FindObjectOfType<Nivel> ().lvl++;
                }
            }
        if (lvl == 4)
            {
            //end
            }


        }
}
