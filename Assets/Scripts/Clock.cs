using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour
{
    public int initialTime;
    
    [Tooltip("Escala del tiempo del reloj")]
    [Range (-10f,10f)]
    public float timeScale;

    private Text text;
    private float frameTimeWithTimescale=0f;
    private float timeInSecondsToShow=0f;
    private float timeScaleWhenPaused, initialTimescale;
    private bool isPaused = false;


    // Start is called before the first frame update
    void Start()
    {
        initialTimescale = timeScale;
        text = GetComponent<Text>();

        timeInSecondsToShow = initialTime;

        RefreshClock(initialTime);
        
    }

    private void RefreshClock(float timeInSeconds)
    {
        int min=0;
        int sec=0;
        string clockText;

        if (timeInSeconds < 0) timeInSeconds = 0;

        min = (int)timeInSeconds / 60;
        sec = (int)timeInSeconds % 60;

        clockText = min.ToString("00") + ":" + sec.ToString("00");

        text.text = clockText;
    }

    // Update is called once per frame
    void Update()
    {
        frameTimeWithTimescale = Time.deltaTime * timeScale;
        timeInSecondsToShow += frameTimeWithTimescale;
        RefreshClock(timeInSecondsToShow);
    }

    public int ReturnMin()
    {
        int min = 0;

        if (timeInSecondsToShow < 0) timeInSecondsToShow = 0;

        min = (int)timeInSecondsToShow / 60;

        return min;
    }

    public int ReturnSec()
    {

        int sec = 0;

        if (timeInSecondsToShow < 0) timeInSecondsToShow = 0;

        sec = (int)timeInSecondsToShow / 60;

        sec = (int)timeInSecondsToShow % 60;

        return sec;

        
    }

    public void Continue()
    {
        if(isPaused)
        {
            isPaused = false;
            timeScale = timeScaleWhenPaused;
        }
    }

    public void Pause()
    {
        if(!isPaused)
        {
            isPaused = true;
            timeScaleWhenPaused = timeScale;
            timeScale = 0;
        }
    }

    public void SetTimeScale(int range)
    {
        timeScale = range;
    }
}
