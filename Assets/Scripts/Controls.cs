using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Controls : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Player player;
    public enum orientation { Left, Right};

    public orientation orientations;

    public void OnPointerDown(PointerEventData eventData)
    {
        if(orientations==orientation.Left)
        {
            player.isLeft = true;
            player.isRight = false;
        }
        if (orientations == orientation.Right)
        {
            player.isRight = true;
            player.isLeft = false;
        }
        //player.isPressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(orientations==orientation.Left)
        {
            player.isLeft = false;
            
        }
        if (orientations == orientation.Right)
        {
            player.isRight = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
