using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerEnterBlocks : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Player")
        {
            GameManager.Instance.Generate();
            Invoke("DeleteBlocks", 7f);
        }
    }

    void DeleteBlocks()
    {
        GameManager.Instance.CreatedPrefabs.Remove(this.transform.parent.GetComponent<BlockCreated>());
        DestroyImmediate(this.transform.parent.gameObject);
    }
        
        
        
}
