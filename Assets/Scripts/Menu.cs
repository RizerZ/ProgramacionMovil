using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public GameObject menuPrincipal;
    public GameObject creditsPanel;




    public void Play()
    {
        SceneManager.LoadScene("Game");
    }

    public void Credits()
    {
        menuPrincipal.SetActive(false);
        creditsPanel.SetActive(true);
    }

    public void Return()
    {
        creditsPanel.SetActive(false);
        menuPrincipal.SetActive(true);
    }

}
