using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shader : MonoBehaviour
{
    public Material material;
    public GameObject player;
    public float valor=1;
    public float velocidad;
    private Material thisMaterial;
    private Player pl;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>().gameObject;
        thisMaterial=GetComponent<Renderer>().material = new Material(material);
        pl = player.GetComponent<Player>();
    }


    private void Update()
    {
        velocidad = (pl.MoveSpeed / 400)-(pl.MoveSpeed/800);
        Debug.Log(velocidad);
        if (player)
            {
            if (this.transform.position.z - player.transform.position.z <= 0.5f && thisMaterial.GetFloat ("_Opacity") >= 0 && player != null)
                {
                valor = valor - (velocidad * Time.deltaTime);
                thisMaterial.SetFloat ("_Opacity", valor);
                }
            }
        
    }
}
