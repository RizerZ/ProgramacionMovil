using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour
{
    public float Speed = 400;

    new Rigidbody rigidbody;

    public Vector3 PlayerVelocity;

    public float MoveSpeed = 200;

    public float Gravity = 1f;

    public bool GameIsStart;


    public bool isLeft;
    public bool isRight;
    //public bool isPressed;

    public Camera cam;

    public Vector3 offset;

    public bool isDead;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        if (GameIsStart)
        {
            cam.transform.position = this.transform.position + offset;

            if(isLeft)
            {
                PlayerVelocity = new Vector3(-MoveSpeed*Time.deltaTime, 0f, Speed * Time.deltaTime);
            }
            else if(isRight)
            {
                PlayerVelocity = new Vector3(MoveSpeed * Time.deltaTime, 0f, Speed * Time.deltaTime);
            }
            else
            {
                PlayerVelocity = new Vector3(0f, 0f, Speed * Time.deltaTime);
            }
            PlayerVelocity.y -= Gravity;
            rigidbody.velocity = PlayerVelocity;

            accelerometer ();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "KillZone")
        {
            isDead = true;
            Invoke("Kill", 0.2f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "KillZone")
        {
            isDead = true;
            Invoke("Kill", 0.2f);
        }

        if(other.tag=="Collectable")
        {
            GameManager.Instance.RefreshScore();
            Destroy(other.gameObject);
        }
    }

    void Kill()
    {
        Destroy(this.gameObject);
    }

    void accelerometer ()
        {
        Vector3 dir = Vector3.zero;
        dir.y =  (Input.acceleration.y);
        dir.x =  (Input.acceleration.x);
        dir.z =  (Input.acceleration.z);
        if (dir.sqrMagnitude > 2)
            {

            rigidbody.AddForce (0, 50, 0, ForceMode.Impulse);
            rigidbody.AddForce (0, 0, 15, ForceMode.Impulse);

            }
        }


    }
